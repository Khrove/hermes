// Set up our HTTP request
var xhr = new XMLHttpRequest();

// Setup our listener to process completed requests
xhr.onload = function () {

    // Process our return data
    if (xhr.status >= 200 && xhr.status < 300) {
        // What do when the request is successful
        console.log('success!', xhr);
    } else {
        // What do when the request fails
        console.log('The request failed!');
    }

    // Code that should run regardless of the request status
    console.log('This always runs...');
};

// Create and send a GET request
// The first argument is the post type (GET, POST, PUT, DELETE, etc.)
// The second argument is the endpoint URL
xhr.open('GET', `https://api.adzuna.com/v1/api/property/gb/search/1?app_id=2e24c9f0&app_key=3e37714ee632db8b856fc79e075718bd`);
xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
xhr.send();
